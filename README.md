---
license: cc-by-4.0
task_categories:
- text-generation
tags:
- code
pretty_name: CodeAlpaca 20K
size_categories:
- 10K<n<100K
language:
- en
---